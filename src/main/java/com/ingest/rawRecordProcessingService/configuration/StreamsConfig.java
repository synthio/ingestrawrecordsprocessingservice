package com.ingest.rawRecordProcessingService.configuration;

import com.ingest.rawRecordProcessingService.streams.RawRecordsStreams;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(RawRecordsStreams.class)
public class StreamsConfig {
}
