package com.ingest.rawRecordProcessingService.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface RawRecordsStreams {
    String INPUT = "parsedRecords-in";
    String OUTPUT = "processedrecords-out";

    @Input(INPUT)
    SubscribableChannel inboundRecords();

    @Output(OUTPUT)
    MessageChannel outboundRecords();
}
