package com.ingest.rawRecordProcessingService;

import com.mongodb.AuthenticationMechanism;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication
@ComponentScan(basePackages = "com.ingest.rawRecordProcessingService")
public class RawRecordProcessingServiceApplication {
	@Value("${mongo.credentials.database}")
	private String mongoCredentials;

	@Value("${mongo.username}")
	private String mongoUsername;

	@Value("${mongo.password}")
	private String mongoPassword;

	@Value("${mongo.url}")
	private String mongoHost;

	public static void main(String[] args) {
		SpringApplication.run(RawRecordProcessingServiceApplication.class, args);
	}

	@Bean
	public MongoClient mongoClient() {
		MongoCredential credential = MongoCredential.createCredential(mongoUsername, mongoCredentials, mongoPassword.toCharArray())
				.withMechanism(AuthenticationMechanism.SCRAM_SHA_1);


		MongoClient mongoClient = MongoClients.create(
				MongoClientSettings.builder()
						.applyToClusterSettings(builder ->
								builder.hosts(Arrays.asList(new ServerAddress(mongoHost, 27017))))
						.credential(credential)
						.build());

		return mongoClient;
	}

}
