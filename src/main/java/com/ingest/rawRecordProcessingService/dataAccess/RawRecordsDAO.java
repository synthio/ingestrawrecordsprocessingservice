package com.ingest.rawRecordProcessingService.dataAccess;

import com.ingest.rawRecordProcessingService.models.ParsedRecord;
import com.mongodb.client.MongoClient;
import com.mongodb.client.model.InsertOneModel;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.lang.reflect.Field;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RawRecordsDAO {

    private MongoClient mongoClient;

    @Autowired
    public RawRecordsDAO(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public Flux<Document> saveRawRecords(String vendorName, String dataSet, List<ParsedRecord> records) {
        List<InsertOneModel<Document>> recordsToSave = createBulkObjects(records);

        this.mongoClient.getDatabase(vendorName)
                .getCollection(dataSet)
                .bulkWrite(recordsToSave);

        return Flux.fromIterable(recordsToSave
                .stream()
                .map(x -> x.getDocument())
                    .collect(Collectors.toList()));
    }

    private List<InsertOneModel<Document>> createBulkObjects(List<ParsedRecord> records) {
        return records
                .stream()
                .map(this::createBulkInsertObject)
                .collect(Collectors.toList());
    }

    private InsertOneModel<Document> createBulkInsertObject(ParsedRecord record) {
        try {
            return this.transformToDocument(record);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return new InsertOneModel<Document>(new Document());
    }

    private InsertOneModel<Document> transformToDocument(Object record) throws IllegalAccessException {
        Document doc = new Document();
        for (Field field : record.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            doc.append(field.getName(), field.get(record));
        }
        doc.append("createdDateDTTM", Date.from(Instant.now()));
        return new InsertOneModel(doc);
    }
}
