package com.ingest.rawRecordProcessingService.models;

import lombok.*;

import java.util.UUID;

@EqualsAndHashCode
@ToString
@Getter
@Setter
@Data
public class ParsedRecord {
    private String dataSet;
    private String vendorName;
    private UUID transactionId;
    private String fileFormat;
    private String createdDate;
    private String rowValue;
}
