package com.ingest.rawRecordProcessingService.listeners;

import com.ingest.rawRecordProcessingService.dataAccess.RawRecordsDAO;
import com.ingest.rawRecordProcessingService.models.ParsedRecord;
import com.ingest.rawRecordProcessingService.streams.RawRecordsStreams;
import lombok.extern.log4j.Log4j2;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.util.Base64;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static reactor.core.publisher.Flux.merge;

@Component
@Log4j2
public class ParsedRecordListener {

    private RawRecordsDAO rawRecordsDAO;

    @Autowired
    public ParsedRecordListener(RawRecordsDAO rawRecordsDAO) {
        this.rawRecordsDAO = rawRecordsDAO;
    }

    @StreamListener
    @Output(RawRecordsStreams.OUTPUT)
    public Flux<Document> handleParsedRecords(@Input(RawRecordsStreams.INPUT) Flux<ParsedRecord> parsedRecords) {
        return merge(
                parsedRecords
                    .parallel()
                    .runOn(Schedulers.parallel())
                    .map(this::encodeRawValue)
                    .sequential()
                    .buffer(1000)
                    .parallel()
                    .runOn(Schedulers.parallel())
                    .map(this::saveBufferedRecords));

    }

    private Flux<Document> saveBufferedRecords(List<ParsedRecord> record) {
        Map<String, List<ParsedRecord>> groups = record
                .stream()
                .collect(groupingBy(rec -> rec.getVendorName() + ";" + rec.getDataSet()));
        Flux<Document> savedRecords = Flux.empty();

        for(String key : groups.keySet()) {
            String[] groupKeys = key.split(";");
            String vendorName = groupKeys[0];
            String dataset = groupKeys[1];
            savedRecords = Flux.merge(savedRecords, this.rawRecordsDAO
                    .saveRawRecords(vendorName, dataset, groups.get(key)));
        }

        return savedRecords;
    }

    private ParsedRecord encodeRawValue(ParsedRecord record) {
        record.setRowValue(Base64.getEncoder().encodeToString(record.getRowValue().getBytes()));
        return record;
    }
}
