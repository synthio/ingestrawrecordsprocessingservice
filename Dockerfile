FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
EXPOSE 9092
COPY target/*-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]